"""

author: alperyildirim@posteo.de

"""

import os
import json
import csv
from jsonschema import validate
from collections import Counter
import logging

# region Global Variables
output_file = os.getenv('output_file')
input_file = os.getenv('input_file')
schema_file = os.getenv('schema_file')
log_file = os.getenv('log_file')

with open(schema_file, 'r') as f:
    schema_str = f.read()

schema = json.loads(schema_str)
logging.basicConfig(filename=log_file, level=logging.DEBUG)

# endregion


def validate_schema(input):
    """
    takes a json object and validates if the data acts according to the schema
    :param input: json object as str
    :return: bool
    """
    global schema
    try:
        validate(instance=input, schema=schema)
        return True
    except Exception as e:
        logging.info(f"schema validation error on row: {input} with the following exception: {e}")
        pass


def aggregate_events(row):
    """
    :param row: row as json data
    :return: value of the key
    """
    return row["event"]


def read_json_data_by_row(file_path):
    """
    Input stream data is read by row and generated for on-the-fly memory consumption.
    Schema is also validated on the fly.
    :param file_path:
    :return:
    """
    with open(file_path, 'r') as f:
        for line in f:
            if validate_schema(input=str(line.strip())):
                yield json.loads(str(line.strip()))


def generate_report(output_file, dict_data):
    """
    outputs aggregated result into csv file
    :param output_file:
    :param dict_data:
    :return:
    """
    csv_columns = ["event_name", "count"]
    try:
        with open(output_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for key in dict_data:
                writer.writerow(
                    {"event_name": key, "count": dict_data[key]}
                )
    except IOError as e:
        logging.info(f"I/O error with the following exception: {e}")


if __name__ == '__main__':
    logging.info("+++++++++++++++ start +++++++++++++++")

    row = read_json_data_by_row(file_path=input_file)

    transformed_result = map(aggregate_events, row)

    aggregated_result = Counter(transformed_result)

    generate_report(
        output_file=output_file, dict_data=aggregated_result)

    logging.info("+++++++++++++++ end +++++++++++++++")
