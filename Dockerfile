FROM python:3.6

COPY requirements.txt  /home
RUN pip install -r /home/requirements.txt

RUN mkdir -p /home/data
# defining env variables to be used later on inside the code
ENV input_file /home/data/input.json
ENV output_file /home/output/result.csv
ENV schema_file /home/schema/schema.json
ENV log_file /home/logs/schema.logs

RUN mkdir -p /home/script
RUN mkdir -p /home/output
RUN mkdir -p /home/schema
WORKDIR /home