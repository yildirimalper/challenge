build-image: # Build the docker image before running
	@docker build -t coding:challenge .


run-container:
	@docker run -t -d \
        --name task \
        -v "$(shell pwd)"/data:/home/data \
        -v "$(shell pwd)"/output:/home/output \
        -v "$(shell pwd)"/schema:/home/schema \
        -v "$(shell pwd)"/logs:/home/logs \
        -v "$(shell pwd)":/home/script \
        coding:challenge


exec-script:
	@docker exec task python3.6 /home/script/main.py

# stopping - removing docker containers and images
stop-container:
	@docker stop task

remove-container:
	@docker rm task

remove-docker-image:
	@docker rmi coding:challenge